#task_1
def my_function(arg1,arg2):
    if type(arg1) == int and type(arg2) == int :
        return arg1 * arg2
    elif type(arg1) == str and type(arg2) == str:
        return arg1 + arg2
    elif type(arg1) == str and type(arg2) != str :
        return dict({arg1 : arg2})
    else :
        return tuple((arg1,arg2))
print (f'result is {my_function(arg1= 1 , arg2= "hello" )}')

#task_2
from random import randint
x = randint(0,1)
print(x)

def my_random(numb):
    if numb != x :
        return "repeat again"
    elif numb == x :
        my_answer = (input("Correct! Do you want repeat (yes|no):"))
        if "no" in my_answer:
            return "the end"
        if "yes" in my_answer:
            return "repeat the game"
print(my_random(numb=0))

