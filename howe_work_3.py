#task_1
number = '2'
my_word = 'Hillel'

#the_first_option
my_str = 'The'+ ' ' + number + ' ' + 'symbol in' + ' ' + my_word + ' ' + 'is i'
print(my_str)

#the_second_option
my_str = 'The {} symbol in {} is i'
my_str = my_str.format(number,my_word)
print(my_str)

#task_3
lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']

lst2 = []
for i in lst1:
    if type(i) == str:
        lst2.append(i)
print(lst2)

#task_2 (в консоле написал, тут продублировал для себя)

my_str = 'Hello and who is more objective ?'
print(my_str.split(' '))
print(my_str.count('o', 0, 13))

#по циклу for я разобрал много вариантов, которые пришли в голову, но сделать так, чтоб на  ти о в конце не получилось.
#my_str = 'Hello and who is more objective ?'
#print(my_str.split(' '))
#for i in my_str:
#    if i == 'o':
#       print(my_str.count('o', 3, 13 ))
#print(my_str.count('o'))
